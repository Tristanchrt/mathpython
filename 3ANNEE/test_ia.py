# MACHINE LEARNING DEBUT 

On commence a faire le lien avec les variables 
-- elles-memes 
-- variables target
-- variables variables selon leurs categories 
-- nan analyse
-- tester hypothese

TOUJOURS ESSAYER DE CREER DES NOUVELLES VARIABLES QUI PEUVENT ETRE INTERSSANTE

TESTER CHAQUE VARIABLE PAR RAPPORT A TOUTES LES AUTRES ET TRES INTERESSANT ON PEUT UTILISER :
La matrice de correlation ou pairplot sns

# NON SUPERVISER TECHNIQUE

clustering pour partitionnement de données 
Isolation forest pour detection d erreur
Reduction de dimension ligne 438 a 462

#####################
VISUALISER LES DONNEES

boston = load_boston()

print(boston.DESCR)
bos.describe()
bos = pd.DataFrame(boston.data)
bos.columns = boston.feature_names
bos['PRICE'] = boston.target
bos.head()
bos.info()
bos.isnull().sum()
bos.corr()
bos.dtypes
pd.set_option('display.max_row', 111) # afficher column
pd.set_option('display.max_column', 111)

diamonds.sample(n = 15, random_state=5) # random line

for col in covid.select_dtypes('float'): # DISTRIBUTION DES VARIABLES FLOAT D'UN DATASET
    plt.figure()
    sns.distplot(covid[col])

for col in covid.select_dtypes('object'): # VARIABLE DE CATEGORIE
    print(f'{col :-<50}{covid[col].unique()}')

def distribGlob(row, colum, data): 
    fig, ax = plt.subplots(nrows=row, ncols=colum, figsize=(16,4))
    col = data.columns
    index = 0

    for i in range(row):
        for j in range(colum):
            sns.distplot(data[col[index]], ax=ax[i][j]) # distribution
            index += 1

    plt.tight_layout() 

fig, ax = plt.subplots(figsize=(18, 12))  # MATRICE DE CORRELATION
sns.heatmap(bos.corr(), annot=True, annot_kws={'size': 12})

sns.distplot(y_test, kde_kws={"label": "Test"})
sns.distplot(y_predict, kde_kws={"label": "Predict"})

err_hist = np.abs(y_test - y_predict) # histo des prediction regression lineaire
plt.hist(err_hist, bins=100)
plt.show()

f, ax = plt.subplots(figsize=(10, 6))

sns.scatterplot(x='=> PRICE', y='RM', data=bos, hue='DIS') # avec point
sns.relplot(x="total_bill", y="tip", hue="smoker", col="sex", row="time", data=tips); # avec les points # kind='line' pour faire en ligne
sns.violinplot(x=tips['sex'], y=tips['total_bill']); # comme un violon
sns.catplot(x='day', y='total_bill', hue='smoker', kind='bar', data=tips) # kind = box, bar # categorie
sns.countplot(x=tips['size'], data=tips); # DES BARRES COUNT
sns.barplot(x="alcohol", y="abbrev", data=crashes, color="b") # DES BARRES
sns.jointplot(x=tips['total_bill'], y=tips['tip'], data=tips, kind="hex"); # kind = 'kde' ou 'hex' # deux tableaux
sns.pairplot(mpg, hue="origin", vars=["mpg", "horsepower", "weight", "acceleration", "displacement"]) # prendre ce qu'on veut 
sns.distplot(y_test, kde_kws={"label": "Test"}) # distribution


for col in blood_columns: # AFFICHER DISTRIBUTION SELON DES CATEGORIES D'UNE COLONNES D'UN DATASET
    plt.figure()
    for cat in covid['status'].unique():
        sns.distplot(covid[covid['status'] == cat][col], label=cat)
    plt.legend()

# PRE-PROCESSING 

pd.DataFrame(model.feature_importances_, index=X_train.columns).plot.bar(figsize=(12,8))

code = { 'positive':1,
    'negative':0,
    'detected:':1,
    'not_detected':0 }
for col in covid.select_dtypes('object'):
    covid[col] = covid[col].map(code)

# PLUSIEURS TABLEAUX
g = sns.FacetGrid(tips, col="time", hue="smoker", height=5)
g.map(sns.distplot, "total_bill");
g.set_axis_labels("Total bill (US Dollars)");
g.add_legend()

fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(16,4), squeeze=False)
sns.violinplot(x="carat", y="cut", data=diamonds, ax=ax[0][0]); 
sns.violinplot(x="carat", y="color", data=diamonds, ax=ax[0][1]); 
sns.violinplot(x="carat", y="clarity", data=diamonds, ax=ax[0][2])
plt.tight_layout() 

#######################

STATS PANDAS

tips['size'].value_counts() # OP on peut mettre .index pour get les index
tips['total_bill'].hist()
tips['sex'].value_counts().plot.bar()
tips.groupby(['sex', 'smoker']).mean()
tips[tips['sex'] == 1].groupby(['smoker']).mean()
loc and iloc tips.loc[0:2, ['size', 'sex']] tips.iloc[0:2, 0:2]
tempMajCity['1800':'2015'].groupby(['City']).agg(['mean', 'std', 'max','min'])
shootIndc.groupby(['state_name'])['year'].agg(['count'])
covid.dtypes.value_counts()
pd.crosstab(covid['Influenza A'], covid['Influenza A, rapid test'])
covid['status'].unique() # type de categorie
#######################

TIMESERIES

tempMajCity[datedeb:'2015'].groupby(['City'])['temp'].plot(figsize=(18, 8))
tempMajCity[tempMajCity['City'] == 'Toronto'][datedeb:'2015']['temp'].rolling(window=7, center=True).mean().plot(label='moving average Toronto', lw=2, ls='--')
# tempMajCity['2000':'2015']['temp'].resample('M').mean().plot(label='moyenne des villes',lw=2 ,ls='--', alpha=0.5)
plt.legend()
plt.show()



import numpy as np
import matplotlib.pyplot as plt 
from sklearn.linear_model import LinearRegression 
from sklearn.neighbors import KNeighborsClassifier 

np.random.seed(0)
m = 100
X = np.linspace(0, 10, m).reshape(m, 1)
y = X + np.random.randn(m, 1)


model = LinearRegression()
model.fit(X, y)
model.score(X, y)
prediction = model.predict(X)

plt.scatter(X, y)
plt.plot(X, prediction, c='r')

# PREMIER PREDICTION 

titanic = sns.load_dataset('titanic')
titanic = titanic[['survived', 'pclass', 'sex', 'age']]
titanic.dropna(axis=0, inplace=True)
titanic['sex'].replace(['male', 'female'], [0, 1], inplace=True)


model = KNeighborsClassifier()
y = titanic['survived']
X = titanic.drop('survived', axis=1)

model.fit(X, y)
model.score(X, y)

def survive(model, pclass=3, sex=0, age=20):
    x = np.array([pclass, sex, age]).reshape(1, 3)
    print(model.predict(x))
    print(model.predict_proba(x))

survive(model)

# MODEL SELECTION : Train_test_split, Cross Validation, GridSearchCV 

import numpy as np
import matplotlib.pyplot as plt 
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import validation_curve
from sklearn.neighbors import KNeighborsClassifier 
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import learning_curve

iris = load_iris()
X = iris.data
y = iris.target

params_grid = {'n_neighbors' : np.arange(1, 20), 'metric': ['euclidean', 'manhattan']}

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)
grid = GridSearchCV(KNeighborsClassifier(), params_grid, cv=5)

grid.fit(X_train, y_train)

# grid.best_score_
model = grid.best_estimator_

# confusion_matrix(y_test, model.predict(X_test))
N, train_score, val_score = learning_curve(model, X_train, y_train, train_sizes=np.linspace(0.1, 1.0, 10), cv=5)


plt.plot(N, train_score.mean(axis=1), label='train')
plt.plot(N, val_score.mean(axis=1), label='validation')
plt.xlabel('train_sizes')
plt.legend()
# model = KNeighborsClassifier(n_neighbors=6)


model.fit(X_train, y_train)
model.score(X_test, y_test)

def predict(model, sp=6.0, sw=2.7, pl=6.6, pw=2.0):
    x = np.array([sp, sw, pl, pw]).reshape(1, 4)
    print(model.predict(x))
    print(model.predict_proba(x))

predict(model)
print(model.score(X_test, y_test))


import numpy as np
import matplotlib.pyplot as plt 
from sklearn.metrics import *
from sklearn.linear_model import LinearRegression
from sklearn.datasets import load_boston

y = np.array([1,2,45,12,26,1000])
y_pred = np.array([5,2,6,8,9,10])

print('MAE', mean_absolute_error(y, y_pred)) # POUR VOIR LES ERREURS 
print('MSE', mean_squared_error(y, y_pred)) # POUR VOIR LES ERREURS 
print('RMSE', np.sqrt(mean_squared_error(y, y_pred))) # POUR VOIR LES ERREURS 
print('MEDIANE ABSOLUTE ERROR', median_absolute_error(y, y_pred)) # POUR VOIR LES ERREURS 

err_hist = np.abs(y - y_pred) # histo des prediction regression lineaire
plt.hist(err_hist, bins=100)
plt.show()

# ENCODAGE

y = np.array(['c', 'fd', 'ffd', 'fdfdfd', 'dsdsdf', 'ssfdsdffs'])
encoder = LabelEncoder() # pour compressere LabelBinarizer() 
encoder.fit_transform(y) # encode 
# encoder.inverse_transform(np.array([0,0,1,2])) # decode

# NORMALISATION 

iris = load_iris()
X = iris.data

X_minmax = MinMaxScaler().fit_transform(X) # Pour que chaque variable soit entre 0 et 1
X_stdscl = StandardScaler().fit_transform(X) # Standardise chaque variable X : La moyene esst null et l'ecart type type egale 1
X_robscl = RobustScaler().fit_transform(X) # quand outilers
# MAIS PROBLEMATIQUE SI OUTLIERS ( SENSIBLE DONNEE ABERANTE )

plt.scatter(X[:, 2], X[:, 3])
plt.scatter(X_minmax[:, 2], X_minmax[:, 3])
plt.scatter(X_stdscl[:, 2], X_stdscl[:, 3])
plt.scatter(X_robscl[:, 2], X_robscl[:, 3])


# PIPLINE 

import numpy as np
import matplotlib.pyplot as plt 
from sklearn.preprocessing import *
from sklearn.datasets import load_iris
from sklearn.pipeline import *
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import GridSearchCV

iris = load_iris()
X = iris.data
y = iris.target

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

model = make_pipeline(PolynomialFeatures(), StandardScaler(), SGDClassifier(random_state=0))

params = {
    'polynomialfeatures__degree' : [2,3,4],
    'sgdclassifier__penalty' : ['l1', 'l2']
}

grid = GridSearchCV(model, param_grid=params, cv=4)

grid.fit(X_train, y_train)
model = grid.best_estimator_

model.score(X_test, y_test)

# for param in rnd_search.get_params().keys(): POUR TROVUER LES PARAMETRES
#     print(param)

# PIPILINE AVANCER

titanic = sns.load_dataset('titanic')

y = titanic['survived']
X = titanic.drop('survived', axis=1)

params_grid = {'kneighborsclassifier__n_neighbors' : np.arange(1, 10), 'kneighborsclassifier__metric': ['euclidean', 'manhattan']}

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

n_features = ['pclass', 'age', 'fare'] # on peut aussi make_column_selector(dtype_include=np.number)
c_features = ['sex', 'deck', 'alone'] # make_column_selector(dtype_exclude=np.number)

n_pipline = make_pipeline(SimpleImputer(), StandardScaler())
c_pipline = make_pipeline(SimpleImputer(strategy='most_frequent'), OneHotEncoder())

prepocessor = make_column_transformer((n_pipline, n_features),(c_pipline, c_features))

model = make_pipeline(prepocessor, KNeighborsClassifier())

grid = GridSearchCV(model, param_grid=params_grid, cv=5)
grid.fit(X_train, y_train)

# for param in rnd_search.get_params().keys(): POUR TROVUER LES PARAMETRES
#     print(param)

model = grid.best_estimator_
model.score(X_test, y_test)


# IMPUTER TRANSFORMER POUR LES DONNNES 

import numpy as np
import matplotlib.pyplot as plt 
import seaborn as sns
import pandas as pd
import scipy as sp 
from math import *
from sklearn import tree
from sklearn.datasets import load_iris
from sklearn.impute import *

X = np.array([[10,3], [0,4], [5,3], [np.nan, 3]])

imputer = SimpleImputer(missing_values=np.nan, strategy='mean')

imputer.fit_transform(X)


titanic = sns.load_dataset('titanic')
X = titanic[['pclass', 'age']]
y = titanic['survived']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

model = make_pipeline(KNNImputer(), SGDClassifier())

params = { 'knnimputer__n_neighbors' : [1,2,3,4,5]}

grid = GridSearchCV(model, param_grid=params, cv=5)

grid.fit(X_train, y_train)

grid.best_params_



# SELECTION DE VARIABLE SELECTOR

from sklearn.feature_selection import *
from sklearn.feature_selection import SelectKBest, chi2

plt.plot(X)
plt.legend(iris.feature_names)

X.var(axis=0)

selector = VarianceThreshold(threshold=0.2)
selector.fit_transform(X)

np.array(iris.feature_names)[selector.get_support()]


selector = SelectKBest(chi2, k=2) # chie2, annavo test de dependance
selector.fit_transform(X, y)
selector.get_support()


selector = SelectFromModel(SGDClassifier(random_state=0), threshold='mean')
selector.fit_transform(X, y)
selector.get_support()

selector.estimator_.coef_.mean(axis=0)

selector = RFECV(SGDClassifier(), step=1, min_features_to_select=2, cv=5) # tres sympa
selector.fit(X, y)
selector.ranking_
selector.grid_scores_



shootIndc = pd.read_csv('https://raw.githubusercontent.com/plotly/datasets/master/US-shooting-incidents.csv') # faire map avec le temps
shootIndc.drop(['dept', 'canine', 'eow'], axis=1, inplace=True)
 # ENLEVER Cause of Death: DANS CAUSE POUR PLUS PROPRE  ENLEVER Cause of Death: DANS CAUSE POUR PLUS PROPRE  ENLEVER Cause of Death: DANS CAUSE POUR PLUS PROPRE

shotYearNB = np.array(shootIndc['year'].value_counts())
shotYearIndex = shootIndc['year'].value_counts().index

# fig = px.scatter(x=shotYearIndex, y=shotYearNB, size=shotYearNB, hover_name=shotYearNB)

fig = go.Figure(data=[go.Scatter(
    x = shotYearIndex,
    y = shotYearNB,
    mode='markers',
    hovertemplate='Shot by year : %{y:nb.2f} <br>Year : %{x:d3-%Y-%m-%d}<extra></extra>',
    marker = dict(
        showscale=True,
        symbol = 'circle',
        colorscale = 'Viridis', # Viridis  Cividis RdBu
        size = shotYearNB / 5,
        color = shotYearNB,
        cmax = shotYearNB.max(),
        colorbar_title="Nb of shot accident"
    )
)])
# fig.update_traces(mode="markers", hovertemplate=f'{shotYearNB.reshape(33,1)}')
fig.update_layout(
    title='Nomber shot accident by year',
    xaxis=dict(
        title='YEAR',
    ),
    yaxis=dict(
        title='NB OF SHOT',
    )
)
fig.show()


import numpy as np 

# nan function quand dans un tableau il y a des valeurs nan ( Not a Number)

np.random.seed(0)
u = np.random.randint(0, 10, [5,5])
print(u)
print(np.corrcoef(u))
print(u.sum(axis=1)) # 0 = y et 1 = x
values, counts = np.unique(u, return_counts=True) # pour voir les repition
for i, j in zip(values[counts.argsort()], counts[counts.argsort()]):
    print(f'valeur {i} apprait {j}')

A[np.isnan(A)] = 0 # Pour remplace les valeurs nan par 0 car isnan return un masque

import numpy as np
import matplotlib.pyplot as plt 
from sklearn.datasets import load_iris
from mpl_toolkits.mplot3d import Axes3D
from scipy import ndimage
image = plt.imread('the-microscope-18-728.jpg')
image = image[:,:,0]


plt.imshow(image)
image_2 = np.copy(image)
plt.figure()

image_2 = image_2 < 160
open_x = ndimage.binary_opening(image_2)

lbl_img, nb_lbl = ndimage.label(open_x)

sizes = ndimage.sum(open_x, lbl_img, range(nb_lbl))
plt.scatter(range(nb_lbl), sizes, c='orange')



digits = load_digits()
images = digits.images
X = digits.data
y = digits.target

model = IsolationForest(random_state=0, contamination=0.03)
model.fit(X)
outliers = model.predict(X) == 1
y = y[outliers]
X = X[outliers]
images = images[outliers]

model = PCA(n_components=0.96)
X_reduced = model.fit_transform(X)
# plt.plot(np.cumsum(model.explained_variance_)) # Pour la compression des donnees trouver la meilleur divension graphiquement
np.argmax(np.cumsum(model.explained_variance_ratio_) > 0.99) # Pour la compression des donnees trouver la meilleur divension

X_recovered = model.inverse_transform(X_reduced) # remettre l'image en 2d

plt.imshow(X_recovered[0].reshape((8,8)))

# model = PCA(n_components=2)
# X_reduced = model.fit_transform(X)
# plt.scatter(X_reduced[:, 0], X_reduced[:, 1], c=y)
# plt.colorbar()