# -*- coding: utf-8 -*-
"""
Created on Thu Apr  8 10:20:04 2021

@author: Olivier TORREQUADRA
3ICS 21 -- TP2 -- Exo 4 : Proba
"""

"""
Ecrire une fonction qui simule des lancers de piece ́equilibree jusqu’à l’obtention de la suite "face,face,pile "
et qui retourne le nombre de lancers effectués.
Ecrire une fonction qui simule des lancers de pièce ́equilibree jusqu’à  l’obtention de la suite ”pile,face,face" 
et qui retourne le nombre de lancers effectues.
Deux amis A et B jouent au jeu suivant : si la suite "face,face,pile " arrive avant la suite ”pile,face,face" , alors A gagne, 
sinon B gagne.
 
 Ecrire une fonction qui simule l’experience et retourne le nom du gagnant.
  
 Ecrire une fonction prenant en paramètre un entier n, repetant n fois le jeu ci-dessus et retournant la proportion de parties remportees par A
 

"""


import numpy as np
from random import *
import matplotlib.pylab as plt
from math import *

def lancerFFP():
    """
    fonction qui simule des lancers de piece ́equilibree jusqu’à l’obtention de la suite ”pile,pile,face” et qui retourne le nombre de lancers effectués.
    """
    check = True
    liste = [1,0,1]
    j = 0
    while check:
        if liste[0] == 1 or liste[1] == 0 or liste[2] == 0: 
            liste[0] =  randint(0,1)
            liste[1] =  randint(0,1)
            liste[2] =  randint(0,1)
            j += 1
        if liste[0] == 0 and liste[1] == 1 and liste[2] == 1:
            check = False
            break
    return j

print(lancerFFP())

def lancerPFF():
    """
    Ecrire une fonction qui simule des lancers de pièce ́equilibree jusqu’à  l’obtention de la suite”face,pile,pile” 
    et qui retourne le nombre de lancers effectues.
    """
    check = True
    liste = [1,1,0]
    j = 0
    while check:
        if liste[0] == 0 or liste[1] == 1 or liste[2] == 1: 
            liste[0] =  randint(0,1)
            liste[1] =  randint(0,1)
            liste[2] =  randint(0,1)
            j += 1
        if liste[0] == 1 and liste[1] == 0 and liste[2] == 0:
            check = False
            break
    return j

print(lancerPFF())
    
def unePartie():
    """
    Ecrire une fonction qui simule l’experience et retourne le nom du perdant.
    """
    j1 = lancerPFF()
    j2 = lancerFFP()
    if j1 > j2:
        return 1
    else 
        return 2

print(unePartie())
    
def desParties(pNb):
    """
     Ecrire une fonction prenant en paramètre un entier pNb, répétant n fois le jeu ci-dessus.
     A chaque partie le gagnant est inscrit dans une la liste des résultats.
     La fonction retourne une liste à deux valeurs [nbVictoiresDeA,nbDefaitesA]
    """
    total = []
    j1Rep = 0
    j2Rep = 0 
    for i in range(pNb):
        j1 = lancerPFF()
        j2 = lancerFFP()
        if j1 > j2:
            j1Rep += 1
        else
            j2Rep += 1
    return [j1Rep, j2Rep]

print(desParties(500))
  
"""

Programme de test
- Tester toutes fonctions et faire afficher le résultat
"""