# -*- coding: utf-8 -*-
"""
Created on Fri Jan 22 09:09:38 2021

@author: OT


"""
import random as r

class Matrices():

    def __init__(self,pN,pM=[]):
        self.__lstM=pM[:]
        self.__n=pN
        if self.__lstM==[]:
            print("init0")
            #self.lstM=[[0 for i in range(self.__n)] for i in range(self.__n)]
            for i in range(pN):
                self.__lstM.append([])
                for j in range(pN):
                    self.__lstM[-1].append(0)
        self.__triang=[]
    
    def afficher(self):
        for ligne in self.__lstM:
            for colonne in ligne:
                print(colonne,end=" ")
            print("")
    def afficherT(self):
        for ligne in self.__triang:
            for colonne in ligne:
                print(colonne,end=" ")
            print("")
            
    def setVal(self):
        for i in range(self.__n):
            for j in range(self.__n):
                self.__lstM[i][j]=r.randint(0,9)
                
    def setUneVal(self,pL,pC,pVal):
        self.__lstM[pL][pC]=pVal
                
    def getUneVal(self,pL,pC):
        return self.__lstM[pL][pC]
                
    def somme_mat(self,pMatB):
        valRen=Matrices(self.__n)
        valRen.afficher()
       
        for i in range(self.__n):
            for j in range(self.__n):
                valRen.setUneVal(i,j,self.__lstM[i][j]+pMatB.getUneVal(i,j))
        return valRen
    
    def prod_mat(self,pMatB):
        valRen=Matrices(self.__n)
        for i in range(self.__n):
            for j in range(self.__n):
                somme=0
                for k in range(self.__n):
                    somme+=self.__lstM[i][k]*pMatB.getUneVal(k,j)
                valRen.setUneVal(i,j,somme)
        return valRen
    
    def estInversible(self):
        valRen=True
        self.__triang=self.__lstM[:]
        for i,j in zip(range(self.__n-1),range(self.__n-1)):
            lRef=self.__triang[i]
            coef1=lRef[j]#pivot
            for lig in range(i+1,self.__n):
                lDes=self.__triang[lig]
                coef2=lDes[j]
                print(coef1,coef2)
                for ind in range(j,self.__n): 
                    self.__triang[lig][ind]=coef2*lRef[ind]-coef1*lDes[ind]
            for ind in range(self.__n):
                if self.__triang[ind][ind]==0:
                    valRen=False
        return valRen
    
mat1=[[10,11,12],[20,21,22],[30,31,32]]
mat2=[[100,101,102],[200,201,202],[300,301,302]]

mat3=[[0,0,0],[0,0,0],[0,0,0]]

n=len(mat1)

for i in range(0,n,1):
    for j in range(n):
        somme=0
        for k in range(n):
            somme+=mat1[i][k]*mat2[k][j]
        mat3[i][j]=somme
        
matP=Matrices(3,[[1,1,1],[1,-1,1],[-1,1,1]])
matP.afficher()
matP.estInversible()
matP.afficherT()    


l1=[1,2,3]    
l2=l1
l2[1]=10
print(l1)
print(list(range(0,2)))

