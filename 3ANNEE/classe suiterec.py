# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 18:22:36 2021

@author: Raphaël
"""

import numpy as np

class suiterec():
    def __init__(self,pN,premTerm=[],coeff=[]):
        self.n=pN
        self.coeff=coeff
        self.premTerm=premTerm
        if self.coeff==[]:
            self.coeff=[0 for i in range(self.n)]
        if self.premTerm==[]:
            self.premTerm=[0 for i in range(self.n)]
            
    def matAssociée(self):
    #matrice associée à la suite récurrente
        mat=[[0 for i in range(self.n)] for i in range(self.n)]
        mat[0]=self.coeff
        for i in range(1,self.n):
            mat[i][i-1]=1
        return mat
    
    
    def calcTerm(self,k):
        if k==0:
            matFin=np.eye(self.n)  # matrice identité d'ordre n
        else:
            a=np.array(self.matAssociée())
            b=a
            for i in range(k-1):
                b=np.dot(a,b)
            matFin=b
        col=[]
        for i in range(len(self.premTerm)):   # transforme la ligne en colonne
            col+=[[self.premTerm[i]]]            
        Termes=np.dot(matFin,np.array(col))
        return Termes[-1][0]
    
s=suiterec(3,[1,0,1],[1,1,1])
print(s.matAssociée())
for i in range(15):
    print(s.calcTerm(i))
    

    
    


        