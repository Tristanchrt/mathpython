# -*- coding: utf-8 -*-
"""
Created on Thu Apr  8 10:17:08 2021

@author: Olivier TORREQUADRA
3ICS 21 -- TP2 -- Exo 3 : Proba
"""
import numpy as np
from random import *
import matplotlib.pylab as plt
from math import *

"""

    Considerons l’experience aleatoire suivante : on lance un dé ́equilibree jusqu’a obtenir 3 fois 
    desuite la même parité (3 nombres paires ou 3 nombres impairs)
    Ecrire une fonction simulant cette experience aleatoire et retournant le nombre de lancers effectués.
    Ecrire une fonction prenant en parametre un entier natureln non nul, repetant n fois l’experience ci-dessus,  
    et  retournant  le  nombre  moyen  de  lancers  effectues (la  moyenne  des  valeurs  retournees par la premiere fonction).

"""
def lancerDes():
    """
    fonction qui retourne le nombre de lancers effectué
    """
    total = 0
    prev = -1
    i = 0
    while i != 3:
        val = randint(0,1)
        if val == prev:
            i +=1
        prev = val
        total += 1
    return total  

print(lancerDes())    

def nbMoyen(pNb):
    """
    Fonction qui retourne la moyenne des valeurs retournées par lancer() après pNb appels
    pNb : entier : nombre de fois qu'il faut répépeter l'expérience
    """
    avg=0
    summ=0
    for i in range(pNb+1):
        summ += lancerDes()
    avg = summ/pNb
    return avg

print(nbMoyen(10))
    
def gagnant(pRes):
    """
    pRes : liste contenant les résultats des parties précédentes
    Si deux joueurs jouent successivement, afficher le numéro du joueur qui gagne en premier et l'ajoute à la liste pRes.
    Par exemple si lancer retourne 3 : c'est le joueur 1 qui gagne, si elle retourne 8 c'est le joueur 2
    
    La fonction retourne la liste pRes modifiée
    """
    j1 = lancerDes()
    j2 = lancerDes()
    while j1 == j2:
        j1 = lancerDes()
        j2 = lancerDes()
    if(j1 < j2):
        pRes.append(1)
    else:
        pRes.append(2)
    return pRes
    
def gagnants(pNb):
    """
    pNb : entier : réprésente le nombre de parties à réaliser
    Méthode qui stocke dans une liste le gagnant de chaque partie et qui affiche le pourcentage de victoire de chaque joueur
    """

"""

Programme de test
- Tester toutes fonctions et faire afficher le résultat
"""