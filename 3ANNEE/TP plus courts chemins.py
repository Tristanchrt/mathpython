from numpy import *
import matplotlib.pyplot as plt



# Fonction qui permet d'obtenir la matrice des plus courts chemins dans le cas général

def floyd(a):
    for k in range(len(a)):
        b=a
        for i in range(len(a)):
            for j in range(len(a)):
                b[i][j]=min(a[i][j],a[i][k]+a[k][j])
        a=b
    return a
                


# Premier diagramme :
    
a=array([[0,8,5],[3,0,inf],[inf,2,0]])
for k in range(3):
    b=a
    for i in range(3):
        for j in range(3):
            b[i][j]=min(a[i][j],a[i][k]+a[k][j])
    a=b
print(b)


# Deuxième diagramme


a=array([[0,3,8,inf,4],[inf,0,inf,1,7],[inf,4,0,inf,inf],[2,inf,5,0,inf],[inf,inf,inf,6,0]])
for k in range(5):
    b=a
    for i in range(5):
        for j in range(5):
            b[i][j]=min(a[i][j],a[i][k]+a[k][j])
    a=b
print(b)