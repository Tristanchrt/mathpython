# -*- coding: utf-8 -*-
"""
Created on Fri Mar 26 10:24:28 2021

@author: Userlocal
"""

# Partie 3 sur les graphes du chapitre 5

# Exercice 1

import numpy as np

M=np.array([[0,1,1,0],[0,0,0,1],[0,1,0,1],[1,0,0,0]])
N=np.array([[0,1,0,2],[1,0,0,0],[1,0,0,1],[0,1,1,0]])

"""
 On remarque que np.dot(M,M)=N
 Essayez de comprendre pourquoi sur un exemple...
 Du coup, si je veux connaitre le nombre de chemins qui
permettent de passer d'un état à un autre en 3 coups,
je calcule M^3
En 4 coups M^4 etc...
"""

 

