# -*- coding: utf-8 -*-
"""
Created on Tue Apr  6 16:22:37 2021

@author: Oliivier TORREQUADRA
3ICS 21 -- TP2 -- Exo 2 : graphes

"""
"""
Soit le graph orienté suivant

1 vers 2
2 vers 3 et 8
3 vers 4 et 7
4 vers 1 et 6
5 vers 2 et 8
6 vers aucun
7 vers 4 et 6
8 vers 9
9 vers 3 et 7
"""

import numpy as np
from random import *
import matplotlib.pylab as plt
from math import *


class Tp2Exo2():
    def __init__(self):
        
        """
        Méthode qui retourne la matrice d'adjacence de l'exercice
        dans l'attribut privé matA'
        """
        self.__matA = np.zeros((9, 9))
        self.__matA[0, 1] = 1
        self.__matA[1, 2] = 1
        self.__matA[1, 7] = 1
        self.__matA[2, 3] = 1
        self.__matA[2, 6] = 1
        self.__matA[3, 0] = 1
        self.__matA[3, 5] = 1
        self.__matA[4, 1] = 1
        self.__matA[4, 7] = 1
        self.__matA[6, 3] = 1
        self.__matA[6, 5] = 1
        self.__matA[7, 8] = 1
        self.__matA[8, 2] = 1
        self.__matA[8, 6] = 1
        
    def afficher(self):
        print(self.__matA)
        
    def supprimeLiaison (self,pDepart,pArrivee):
        """
        Méthode qui supprime le chemin entre pDepart et pArrivee
        pDepart : entier : numéro du noeud de départ
        pArrivee : entier : numéro du noeud d'arrivée
        
        La méthode retourne 1 si la suppression a pu se faire et 0 si elle était déjà absente et -1 si un des noeud n'existe pas
        """
        long = 5
        pathLong = self.longueurChemin(long)
        neur = self.__matA.shape[0] - 1
        if pDepart > neur or pDepart < 0 or pArrivee > neur or pArrivee < 0:
          return -1
        elif pathLong[pDepart, pArrivee] == 0:
          return 0
        else:
          return 1
        
        
    def longueurChemin(self,pNb):
        """
        pNb : entier représentant la longueur des chemins souhaités
        Méthode qui retourne la matrice contenant les chemins de longueur pNb
        Il faut utilisier ici la fonction dot de numpy
        """
        newA = np.eye(self.__matA.shape[0])
        for i in range(pNb):
            newA = np.dot(newA, self.__matA)
        return newA

    def boucle(self):
        """
        Fonction qui retourne 1 s'il y a une boucle ou 0 sinon
        Pour trouver une boucle il faudra calculer les chemins de taille 1, puis 2, puis 3 jusqu'à nb de noeuds
        """
    
    def cheminAbsent(self,pN1,pN2,pLong):
        """
        Méthode qui retourne True s'il n'existe pas de chemin entre pN1 et pN2 de longueur pLong
        pN1 : entier : numéro du noeud 1
        pN2 : entier : numéro du noeud 2
        pLong : entier ; longueur du chemin
        """
       
    
    def noeudEmeteur(self):
        """
        Méthode qui retourne la liste des noeud qui n'ont pas de sucsesseur
        """
        return np.nonzero(self.__matA.sum(axis=1) == 0)[0]
       

"""
Programme de test (à faire)
- créer un objet
- tester toutes les fonctions avec tous les cas possible
- Il peut être nécessaire de modifier le graph
"""
        
matrix = Tp2Exo2()

print(matrix.afficher())
print("Longueur")
print(matrix.longueurChemin(3))
print("Suppression")
print(matrix.supprimeLiaison(3, 4))
print("Noeud")
print(matrix.noeudEmeteur())

