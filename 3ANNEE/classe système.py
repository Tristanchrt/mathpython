# -*- coding: utf-8 -*-
"""
Created on Thu Mar 25 09:26:10 2021

@author: Raphaël
"""
import numpy as np

class systeme():
    def __init__(self,pN,mat,memb_droite):
        self.n=pN
        self.matrice=mat
        self.col=memb_droite
        
    
    def isCramer(self):
        if np.linalg.det(np.array(self.matrice))!=0:
            return True
        else:
            return False
        
    def resolution(self):
        if self.isCramer()==True:            
            a=np.array(self.matrice)
            b=np.linalg.inv(a)
            sol=np.dot(b,np.transpose(np.array(self.col)))
            return sol
        else:
            return []
       
    
        

sys1=systeme(3,[[1,1,-1],[1,-1,1],[-1,1,1]],[7,-1,4])
sys2=systeme(3,[[1,1,-1],[1,1,1],[1,1,1]],[7,-1,4])

print(sys1.resolution())
print(sys2.resolution())



        