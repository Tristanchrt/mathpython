# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 08:58:20 2021

@author: Olivier TORREQUADRA
3ICS - Séance 3

"""
import random as r

class Matrices():
    def __init__(self,pN,pM=[]):
        self.__lstM=pM[:]
        self.__n=pN
        if self.__lstM==[]:
            print("init0")
            #self.lstM=[[0 for i in range(self.__n)] for i in range(self.__n)]
            for i in range(pN):
                self.__lstM.append([])
                for j in range(pN):
                    self.__lstM[-1].append(0)
    
    def afficher(self):
        for ligne in self.__lstM:
            for colonne in ligne:
                print(colonne,end=" ")
            print("")
            
    def setVal(self):
        for i in range(self.__n):
            for j in range(self.__n):
                self.__lstM[i][j]=r.randint(0,9)
                
    def setUneVal(self,pL,pC,pVal):
        self.__lstM[pL][pC]=pVal
                
    def getUneVal(self,pL,pC):
        return self.__lstM[pL][pC]
                
    def somme_mat(self,pMatB):
        valRen=Matrices(self.__n)
        valRen.afficher()
       
        for i in range(self.__n):
            for j in range(self.__n):
                valRen.setUneVal(i,j,self.__lstM[i][j]+pMatB.getUneVal(i,j))
        return valRen
    
    def prod_mat(self,pMatB):
        valRen=Matrices(self.__n)
        for i in range(self.__n):
            for j in range(self.__n):
                somme=0
                for k in range(self.__n):
                    somme+=self.__lstM[i][k]*pMatB.getUneVal(k,j)
                valRen.setUneVal(i,j,somme)
        return valRen

        
    def estInversible(self):
        valRen=True
        self.__triang=self.__lstM[:]
        for i,j in zip(range(self.__n-1),range(self.__n-1)):
            lRef=self.__triang[i]
            coef1=lRef[j]#pivot
            for lig in range(i+1,self.__n):
                lDes=self.__triang[lig]
                coef2=lDes[j]
                print(coef1,coef2)
                for ind in range(j,self.__n): 
                    self.__triang[lig][ind]=coef2*lRef[ind]-coef1*lDes[ind]
            for ind in range(self.__n):
                if self.__triang[ind][ind]==0:
                    valRen=False
        return valRen
    

class Etudiants():
    def __init__(self):
        self.__matricule=0
        self.__nom=""
        self.__prenom="Olivier"
        self.__lstNotes=[]
        
    def __str__(self):
        valRen=self.__prenom + " " + self.__nom
        return valRen
        
    def setMatricule(self,pMat):
        self.__matricule=pMat
    def setNom(self,pNom):
        self.__nom=pNom
    def afficher(self):
        print("MAtricule :",self.__matricule)
        print("Nom : ",self.__nom)
        
    
        

unEtu=Etudiants()

unEtu.setMatricule(123)
unEtu.setNom("TORREQUADRA")

unEtu.afficher()
print(unEtu)  

matrice1=Matrices(3)
matrice1.setVal()
matrice1.afficher()
matrice2=Matrices(3)
matrice2.setVal()
matrice2.afficher()

matrice3=Matrices(2,[[2,2],[2,2]])
matrice4=Matrices(2,[[1,1],[1,1]])
print("Test Addition")
res=matrice3.somme_mat(matrice4)
res.afficher()
print("Test produit")
res=matrice3.prod_mat(matrice4)
res.afficher()

"""     
lst1=[1,2,3,4,5]
lst2=[[1,2,3],[4,5,6],[7,8,9]]

print(lst2[0])
print(lst2[0][0])
"""



import numpy as np

class suiterec():
    def __init__(self,pN,premTerm=[],coeff=[]):
        self.n=pN
        self.coeff=coeff
        self.premTerm=premTerm
        if self.coeff==[]:
            self.coeff=[0 for i in range(self.n)]
        if self.premTerm==[]:
            self.premTerm=[0 for i in range(self.n)]
            
    def matAssociée(self):
    #matrice associée à la suite récurrente
        mat=[[0 for i in range(self.n)] for i in range(self.n)]
        mat[0]=self.coeff
        for i in range(1,self.n):
            mat[i][i-1]=1
        return mat
    
    
    def calcTerm(self,k):
        if k==0:
            matFin=np.eye(self.n)  # matrice identité d'ordre n
        else:
            a=np.array(self.matAssociée())
            b=a
            for i in range(k-1):
                b=np.dot(a,b)
            matFin=b
        col=[]
        for i in range(len(self.premTerm)):   # transforme la ligne en colonne
            col+=[[self.premTerm[i]]]            
        Termes=np.dot(matFin,np.array(col))
        return Termes[-1][0]
    
s=suiterec(3,[1,0,1],[1,1,1])
print(s.matAssociée())
for i in range(15):
    print(s.calcTerm(i))
    
    
    
    
# EXO2 TP2

# -*- coding: utf-8 -*-
import numpy as np
"""
Created on Tue Apr  6 16:22:37 2021

@author: Oliivier TORREQUADRA
3ICS 21 - TP2 - Exo 2 : graphes

"""
"""
Soit le graph orienté suivant

1 vers 2 et 7
2 vers 3
3 vers 4 et 5
4 vers 2 et 6
5 vers 6
6 vers 3
7 vers 4 et 8
8 vers 6
9 vers 6 et 8
"""


class Tp2Exo2():
    def __init__(self):
        
        """
        Méthode qui retourne la matrice d'adjacence de l'exercice
        dans l'attribut privé matA'
        """
        self.__matA = [
            [0,1,0,0,0,0,1,0,0],
            [0,0,1,0,0,0,0,0,0],
            [0,0,0,1,1,0,0,0,0],
            [0,1,0,0,0,1,0,0,0],
            [0,0,0,0,0,1,0,0,0],
            [0,0,1,0,0,0,0,0,0],
            [0,0,0,1,0,0,0,1,0],
            [0,0,0,0,0,1,0,0,0],
            [0,0,0,1,0,0,0,1,0]
        ]
       
    def afficher(self):
        print(self.__matA)
        
    def ajoutLiaison (self,pDepart,pArrivee):
        """
        Méthode qui ajoute créé le chemin entre pDepart et pArrivee
        pDepart : entier : numéro du noeud de départ
        pArrivee : entier : numéro du noeud d'arrivée
        
        La méthode retourne 1 si l'ajout a pu se faire et 0 si elle était déjà présente et -1 si un des noeud n'existe pas
        """
        if((pDepart < 1 or pDepart > 9) or (pArrivee < 1 or pArrivee > 9)):
            return -1
        if self.__matA[pDepart-1][pArrivee-1] == 1:
            return 0
        else:
            self.__matA[pDepart-1][pArrivee-1] = 1
            return 1
      
        
    def longueurChemin(self,pNb):
        """
        pNb : entier représentant la longueur des chemins souhaités
        Méthode qui retourne la matrice contenant les chemins de longueur pNb
        Il faut utilisier ici la fonction linalg.matrix_power de numpy
        """
        return np.linalg.matrix_power(np.array(self.__matA), pNb)

    
    def boucle(self):
        """
        Fonction qui retourne True s'il y a une boucle ou False sinon
        Pour trouver une boucle il faudra calculer les chemins de taille 1, puis 2, puis 3 jusqu'à nb de noeuds
        """
        
        mat = self.longueurChemin(len(self.__matA))
        for i in range(len(mat)):
            for j in range(len(mat)):
                if mat[i][j] != 0:
                    return True
        return False
     
    
    def cheminExiste(self,pN1,pN2,pLong):
        """
        Méthode qui retourne True s'il existe un chemin entre pN1 et pN2 de longueur pLong
        pN1 : entier : numéro du noeud 1
        pN2 : entier : numéro du noeud 2
        pLong : entier ; longueur du chemin
        """
        mat = self.longueurChemin(pLong)
        if mat[pN1-1][pN2-1] > 0:
            return True
        return False       
    
    def noeudInterdit(self):
        """
        Méthode qui retourne la liste des noeud vers lesquels il n'y a aucun chemin
        """
        ret = []
        mat_1 = self.longueurChemin(1)
        for i in range(len(mat_1)):
            path_to = False
            for j in range(len(mat_1)):
                if(mat_1[j][i] != 0):
                    path_to = True
                    break
            if(path_to == False):
                ret.append(i+1)
        return ret               
            
    

        
obj = Tp2Exo2()
assert obj.boucle() == True
assert obj.noeudInterdit() == [1,9]


import math as m
import matplotlib.pyplot as plt
import numpy as np
from random import *

def monteCarlo(a, b, nbr_point, yMax):
    x = []
    y = []
    diff = b-a
    under_curve = []
    for i in range(nbr_point):
        x.append(rd.random() * diff)
        y.append(rd.random() * yMax)
        if(y[-1] < f(x[-1])):
            under_curve.append(x[-1])
        plt.plot(x, y, "ro")
    return x, y, under_curve

def f(x):
    return m.exp(x)-x

def tracerCourbe(pF,pA,pB,pDt):
    lX=[]
    lY=[]
    for x in np.arange(pA,pB,pDt):
        lX.append(x)
        lY.append(pF(x))
    plt.xlim(pA,pB)
    plt.ylim(min(lY),max(lY))
    plt.plot(lX,lY)
    plt.fill_between(lX, lY, color='g')
                     
def fRectGauche(pF,pA,pB,pNb):
    valRen=0#Naire totale
    dt=(pB-pA)/pNb
    x1=pA
    for i in range(pNb):
        x2=x1+dt
        valRen+=dt*f(x1)
        x1=x2
    return valRen

def tracerRectangle(pF,pA,pB,pNbR):
    lX=np.linspace(pA,pB,pNbR)
    print("LX:",lX)
    for i,x in enumerate(lX[:-1]):
        x2=lX[i+1]
        xRec=[x,x2]
        yRec=[pF(x),pF(x)]
        plt.plot(xRec,yRec,"r")
        
def fTrapeze(pF,pA,pB,pNb):
    valRen=0#◘aire totale
    dt=(pB-pA)/pNb
    x1=pA
    for i in range(pNb):
        x2=x1+dt
        valRen+=dt*(pF(x1)+pF(x2))/2
        x1=x2
    return valRen



tracerCourbe(f, 0, 1, 0.01)
tracerRectangle(f, 0, 1, 18)
# tracerTrapeze(f, 0, 1, 18)
for n in range (2,1000):
    print (n,"rect : ",fRectGauche(f,0,1,n))

def floyd(a):
    for k in range(len(a)):
        b=a
        for i in range(len(a)):
            for j in range(len(a)):
                b[i][j]=min(a[i][j],a[i][k]+a[k][j])
        a=b
    return a


from math import *
import numpy as np
import matplotlib.pyplot as plt
import sympy as sym


def f(a):
    return 3 * a ** 3 - 2 * a ** 2 + a + 2


def g(a):
    return a + 1 / a


def h(a):
    return 2 * log(a) - a + 2


def derivativef(a):
    x = sym.symbols('x')
    f_prime = sym.lambdify(x, sym.diff(3 * x ** 3 - 2 * x ** 2 + x + 2))
    return f_prime(a)


def derivativeg(a):
    x = sym.symbols('x')
    g_prime=sym.lambdify(x,sym.diff(x + 1 / x))
    return  g_prime(a)


def derivativeh(a):
    x = sym.symbols('x')
    h_prime = sym.lambdify(x, sym.diff(2 * log(x) - x + 2))
    return h_prime(a)


def tracer(pF, pMin, pMax, pPas=1, pTitre=""):
    lstX = []
    lstY = []
    for x in np.arange(pMin, pMax + 1, pPas):
        lstX.append(x)
        lstY.append(pF(x))
    plt.title = pTitre
    plt.plot(lstX, lstY)
    plt.show()


def dichotomie(pF, pA, pB, pP):
    valRen = []
    bA = pA
    bB = pB
    if pF(bA) * pF(bB) < 0:
        print("Il y une racine")
        while (abs(bA - bB) > pP):
            milieu = (bA + bB) / 2.0
            fA = pF(bA)
            fM = pF(milieu)
            if fA * fM > 0:
                bA = milieu
            elif fA * fM < 0:
                bB = milieu
            else:
                bA = milieu
                bB = milieu
        print("Intervalle : [", bA, ":", bB, "]")
        valRen = [bA, bB]
    else:
        print("Impossible de calculer")
    return valRen


def newton(func, funcderiv, x, epsilon=0.000001):
    n = 0
    dif = 2 * epsilon
    x1 = 0
    while dif > epsilon:
        x1 = x - func(x) / funcderiv(x)
        dif = abs(x1 - x)
        x = x1
        n = n + 1
    return (x, n)




class Matrice():
    
    def generator_matrice(self, m, n):
        return [[0]*m for i in range(n)]

    def somme_mat(self, matrice2):
        n = self.__matrice
        m = len(matrice2[0]) 
        c = [[matrice[i][j] + matrice2[i][j] for j in range(m)] for i in range(n)]
        return c 

    def prod_mat(self, matrice2):
        n = len(self.__matrice)
        m = len(matrice2[0]) 
        p = len(matrice2) 
        
        new_matrix = [[0]*m for i in range(n)] 
        
        for i in range(n):
            for j in range(m):
                for k in range(p):
                    new_matrix[i][j] += self.__matrice[i][k] * matrice2[k][j]
        return new_matrix

    def is_triang_sup(self, matrice):
        res = 0
        m = len(matrice)
        for i in range(m-1):
            for j in range(i+1, m):
                triangle += matrice[i][j]
        return (triangle == 0) if True else False 

    def is_triang_inf(self, matrice):
        res = 0
        m = len(matrice)
        for i in range(m):
            for j in range(i):
                triangle += M[i][j]
        return (triangle == 0) if True else False 

    def matrice_for_determiant(self,matrix):
        m = self.generator_matrice(len(matrix[0]), len(matrix[0]))
        for i in range(len(matrix[0]) - 1):
            m[i] = matrix[i]
        for i in range(len(matrix[0])):
            matrix += [m[i]]
        return m 

    def determinant_matrice(self, matrix):
        M = matrix
        temp = self.matrice_for_determiant(matrix)
        k = -1
        res = 1
        print(M)
        total_up, total_down = 0, 0
        for l in range(len(M) - 1):
            for i in range(l, len(M)):
                if k == len(M[0]) - 1:
                    total_down = total_down + res
                    k = -1
                    res = 1
                    break
                else:
                    k += 1
                    res *=  M[i][k]

        k = len(M[0])
        res = 1
        for l in range(len(M) - 1):
            for i in range(l, len(M)):
                if k == 0:
                    total_up = total_up - res
                    k = len(M[0]) 
                    res = 1
                    break
                else:
                    k -= 1
                    res *= M[i][k]
        return total_down + total_up

    def com_matrice(self, M): # cofacteur 
        m = len(M[0])
        new_matrix = matrix_sign(M)
        old = M
        for i in range(m):
            for j in range(m):
                M = del_row(M, i)
                M = del_column(M, j)
                res_t_l = trace_left(M)
                res_t_r = trace_right(M)
                new_matrix[i][j] *= (res_t_l - res_t_r)
                M = old
        return self.transposer(new_matrix)

    def transposer(self, matrix, detM): # inverse matrice 
        matrix = [[matrix[j][i] for j in range(len(matrix))] for i in range(len(matrix[0]))]
        m = len(matrix)
        matrix_inverse = self.generator_matrice(m,m)
        for i in range(m):
            for j in range(m):
                matrix_inverse[i][j] = (matrix[i][j] * (1/detM))
        return matrix_inverse 

    def inverse_matrix(matrix): # inverse matrice 
        matrix = [[matrix[j][i] for j in range(len(matrix))] for i in range(len(matrix[0]))]
        m = len(matrix)
        matrix = self.com_matrice(matrix)
        detM = self.determinant_matrice(matrix)
        matrix_inverse = generator_matrice(m,m)
        for i in range(m):
            for j in range(m):
                matrix_inverse[i][j] = (matrix[i][j] * (1/detM))
        return matrix_inverse 

    def cramer(self, M, system, res): # syteme cramer 
        m = len(M[0])
        s = len(system[0])

        matrice = [[0]*s for i in range(s)]
        detA = det_matrix(M)
        res_system = []
        index = 0
        k = 0
        for l in range(index, m): 
            for i in range(m):
                for j in range(m):
                    if l == j:
                        matrice[i][j] = res[k]
                        k += 1
                    else:
                        matrice[i][j] = system[i][j]
            k = 0
            res_system.append(det_matrix(matrice) / detA)
            index += 1
        return res_system