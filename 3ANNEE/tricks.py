import seaborn as sns
import matplotlib.pyplot as plt
from math import *
from textwrap import wrap
import numpy as np
from numpy import random
from Ordinateur import Post, Logiciel, Salle, Utilisateur
from tkinter import *
from tkinter.ttk import *
from pprint import pprint
import statistics
import glob
import random 

xx = [1,2,3,4,5,6,7,8]

yield # pour cree generateur dans une fonction

def createGenerator():
    mylist = range(3)
    for i in mylist:
        yield i*i

for i in createGenerator():
    print(i)

[print(x) for x in xx]

a, _ = (1,2) # declaration du variable qu on ne va utiliser mais obliger a declarer

a, b, *c = (1, 2, 3, 4, 5)  # a, b, *_ = (1,2,3,4,5)
print(f'{a}//{b}//{c}')

num1 = 10_000_000
num2 = 10_000
total = num1 + num2
print(f'{total:,}')

names = ['titi', 'tfofgjodfg', 'sddssdsd', 'fddffdfd']
hero = ['a', 'b', 'c', 'd']

for index, name in enumerate(names, start=1): # start pas obligatoire c'est un foreach ave key
    print(index, name)

for hero, name in zip(hero, names): # start pas obligatoire c'est un foreach ave key
    print(f'{hero} et {name}')

for value in zip(hero, names): # pareil
    print(value[0])


person_info = {'first': 'Corey', 'last': 'shcafer'}

for key, value in person_info.items():
    print(f'{key}//{value}')
    
class Person():
    pass

person = Person()

X.var(axis=0) # VARIENCE AXE TABLEAU

person_info = {'first': 'Corey', 'last': 'shcafer'}

for key, value in person_info.items():
    setattr(person, key, value)


liste_2 = [i**2 for i in range(10)] # LISTE COMPREHENSION
liste_3 = [[i for i in range(3)] for j in range(3)]

prenoms = ['fgfggffg', 'gffgfgfg', 'fdasfdfdfsda', 'sdasdafsda', 'fdsafda']  # DICT COMPREHENSION
age = [1, 545, 454, 45, 665]

dico = {k:v for k, v in enumerate(prenoms)}
dico2 = {prenoms:age for k, v in zip(prenoms, age) if age > 20}


filenames = glob.glob('*.txt')
d = {}
for file in filenames:
    with open(file, 'r') as f:
        d[file] = f.read().splitlines() # recupere une ligne

print(d)


from scipy import misc
import matplotlib.pyplot as plt
face = misc.face(gray=True)
plt.imshow(face, cmap=plt.cm.gray)
plt.show()
print(face.shape)

face = face[75:480, 350:900]

plt.imshow(face, cmap=plt.cm.gray)
plt.show()


import numpy as np 

u = np.random.randint(0, 10, [5,5])
print(u)
u[u < 5] = 55 # BOOLEAN INDEXING
print(u)


from scipy import misc
import matplotlib.pyplot as plt
face = misc.face()
plt.imshow(face)
plt.show()
print(face.shape)

face = face[::2, ::2]

plt.imshow(face)
plt.show()

face = face[face.shape[0] // 4 : -face.shape[0] // 4, face.shape[1] // 4 : -face.shape[1] // 4]

plt.imshow(face)
plt.show()


#GRAPHICS 

import matplotlib.pyplot as plt
import numpy as np 
x = np.linspace(0, 2, 10)
y = x**2

plt.figure(figsize=(8, 5))
plt.plot(x, y, c='red', lw=2.5, ls='--', label='fddffdfdfd')
plt.plot(x, x**3, label='asaaassdsdsd')
plt.title('Figure 1')
plt.xlabel('axe X')
plt.ylabel('axe Y')
plt.legend()
plt.show()
plt.savefig('figure_test.png')

# some grahpics with same image
plt.subplot(2, 1, 1)
plt.plot(x, y, c='red')
plt.subplot(2, 1, 2)
plt.plot(x, x**3, c='green')


# POO 

fig, ax = plt.subplots(2, 1, sharex=True)
ax[0].plot(x ,y)
ax[1].plot(x, np.sin(x))
plt.show()

# c COOL 

dataset = {f"experience{i}": np.random.randn(100, 3) for i in range(4)}

n = len(dataset)
plt.figure(figsize=(10, 8))

for k, i in zip(dataset.keys(), range(1, n+1)):
    plt.subplot(n, 1, i)
    plt.plot(dataset[k])
    plt.title(k)
plt.show()


import numpy as np
import matplotlib.pyplot as plt 
from sklearn.datasets import load_iris # DATA SET IRIS

iris = load_iris() 
x = iris.data 
y = iris.target

names = list(iris.target_names)

print(f'x contient {x.shape[0]} exemples et {x.shape[1]} vairiables')
print(f'il y a {np.unique(y).size} classes')

plt.scatter(x[:, 0], x[:, 1], c=y, alpha=0.5, s=x[:,2]*50)

print(x[:, 0]) # TOUTE LES LIGNES MAIS L'INDEX 1


import numpy as np
import matplotlib.pyplot as plt 
from sklearn.datasets import load_iris
from mpl_toolkits.mplot3d import Axes3D

f = lambda x, y: np.sin(x) + np.cos(x+y)*np.cos(x)
X = np.linspace(0,5,100)
Y = np.linspace(0,5,100)
X, Y = np.meshgrid(X, Y)
Z = f(X,Y)

plt.contour(X, Y, Z, 50, cmap='RdGy')
plt.colorbar()

face = misc.face(gray=True)
plt.imshow(np.corrcoef(x.T), cmap='Blues')
plt.colorbar()

import numpy as np
import matplotlib.pyplot as plt 
from sklearn.datasets import load_iris
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import interp1d

x = np.linspace(0, 10, 10)
y = x**2
plt.scatter(x, y)

f = interp1d(x, y, kind='linear')
new_x = np.linspace(0, 10, 20)
r = f(new_x)

plt.scatter(x, y)
plt.scatter(new_x, r, c='r')


x = np.linspace(-10, 10, 100)
plt.plot(x, fqw(x))

result = optimize.minimize(fqw, x0=-5).x
x0 = -5

plt.scatter(result, fqw(result), s=100, c='r', zorder=1)
plt.scatter(7, 50, s=200, marker='+', c='g', zorder=1) # Mettre un point
plt.show()


x = np.linspace(0, 30, 1000)
y = 3*np.sin(x) + 2*np.sin(5*x) + np.sin(10*x)


fourier = fftpack.fft(y) # fourier descente signal
power = np.abs(fourier)
freq = fftpack.fftfreq(y.size)
plt.plot(np.abs(freq), power)


import numpy as np
import matplotlib.pyplot as plt 
from sklearn.datasets import load_iris
from mpl_toolkits.mplot3d import Axes3D
from scipy import ndimage
image = plt.imread('the-microscope-18-728.jpg')
image = image[:,:,0]

image_2 = np.copy(image)


image = image < 160
open_x = ndimage.binary_opening(image)

lbl_img, nb_lbl = ndimage.label(open_x)

sizes = ndimage.sum(open_x, lbl_img, range(nb_lbl))
plt.scatter(range(nb_lbl), sizes, c='orange')

# PANDAS / PANDAS

import numpy as np
import matplotlib.pyplot as plt 
import pandas as pd


data = pd.read_excel('titanic3.xls')
data = data.drop(['ticket', 'name', 'sibsp', 'parch', 'fare', 'cabin', 'embarked', 'boat', 'body', 'home.dest'], axis=1)
data = data.dropna(axis=0)
print(data.describe())
data.head()

data['age'].hist()
data.groupby(['sex', 'pclass']).mean()
print(data.iloc[0:10, 0:4])
print(data.loc[0:5, 'age'])

data.loc[data['age'] <= 20, 'age'] = 0
data.loc[(data['age'] > 20) & (data['age'] <= 30), 'age'] = 1
data.loc[(data['age'] > 30) & (data['age'] <= 40), 'age'] = 2
data.loc[data['age'] > 40, 'age'] = 3
print(data.iloc[0:10, 0:4])
print(data.loc[0:5, 'age'])

# data['sex'] = data['sex'].map({'male':0, 'female':1})
data['sex'] = data['sex'].astype('category').cat.codes
data.head()

# BITCOIN

bitcoin = pd.read_csv('BTC-EUR.csv', index_col='Date', parse_dates=True)
bitcoin['2020']['Close'].plot(label='Normal', c='green')
bitcoin['2020']['Close'].resample('W').mean().plot(label='moyenne par semaine')
bitcoin['2020']['Close'].resample('M').mean().plot(label='moyenne par mois', lw=3, ls='--', alpha=0.8)
plt.legend()

m = bitcoin['Close'].resample('W').agg(['mean', 'std', 'min', 'max'])
plt.figure(figsize=(12, 8))
m['mean']['2019'].plot(label='moyenne par semaine')
plt.fill_between(m.index, m['min'], m['max'], alpha=0.2, label='min et max par semaine')
plt.legend()


# SEABORN 

import numpy as np
import matplotlib.pyplot as plt 
import pandas as pd
import seaborn as sns

# titanic = sns.load_dataset('titanic')
# titanic.drop(['alone', 'alive', 'who', 'class', 'adult_male', 'embark_town'], axis=1, inplace=True)
# titanic.dropna(axis=0, inplace=True)

# sns.pairplot(titanic)
iris = pd.read_csv('iris.csv')
sns.pairplot(iris, hue='variety')

#sns.catplot(x='pclass', y='age', data=titanic, hue='sex')
sns.jointplot('age', 'fare', data=titanic, kind='hex')

# sns.heatmap(titanic.corr())


