# -*- coding: utf-8 -*-
"""
Created on Mon Apr  5 13:36:40 2021

@author: Olivier TORREQUADRA

3ICS 21 -- TP2 -- Exo 1 : Fonctions
"""

import numpy as np
from random import *
import matplotlib.pylab as plt
from math import *

def fExo1(x):
    """
    fonction qui renvoie l'image de x avec la fonction
    -4x^2+10x+100
    """
    return (-4 * (x*x)) + (10 * x) + 100

print(fExo1(6))

def fExo1Prime(x):
    """
    fonction dérivée de fExo1
    A Faire
    """
    return (-8 * x ) + 10 


print(fExo1Prime(6))

def tracer(pF, pFP, pInt, pPas):
    lstX = []
    lstY = []
    for x in np.arange(pInt[0], pInt[1], pPas):
        lstX.append(x)
        lstY.append(pF(x))
    plt.plot(lstX, lstY, 'g--')
    lstX = []
    lstY = []
    for x in np.arange(pInt[0], pInt[1], pPas):
        lstX.append(x)
        lstY.append(pFP(x))
    plt.plot(lstX, lstY, 'b--')
    plt.show()

tracer(fExo1(6),fExo1Prime(6), [-100, 100], 1 )


def fSeparerInterval(pInt,pNbI):
    """
    pInt : liste de deux valeur représentant un interval
    pNbI : nombre d'intervals à obtenir
    Faire une fonction qui tronconne l'interval en x interval plus petit et renvoie la liste des intervals'
    exemple : pInt=[-10,10], pNbI=2 --> [[-10,0],[0,10]]
    exemple : pInt=[-10,10], pNbI=4 --> [[-10, -5.0], [-5.0, 0.0], [0.0, 5.0], [5.0, 10]]
    """
    inter = pInt[1] - pInt[0]
    pas = inter / pNbI
    total = []
    for x in range(pNbI):
        liste = [pInt[0]+pas,pInt[0]+pas(i+1)]
        total.append(liste)
    return total

print(fSeparerInterval([-10, 10], int(3)))

def fRacine(pF,pI):
    """
    pF : fonction (c'est la fonctio fExo1 ici)
    pI : liste représentant l'interval 
    Faire une fonction qui retourne 1 s'il y a une racine de pF sur pI
    ou 0 si non ou si impossible à savoir avec cet interval
    """
    val = pF(pI[0]) > 0
    for i in np.arange(pI[0], pI[1], 1):
        if(pF(i) > 0 != val):
            return 1
    return 0

def fRacines(pF,pLstI):
    """
    pF : fonction
    pLstI : liste contenant les intervals à tester
    exemple de pLstI : [[-10, -5.0], [-5.0, 0.0], [0.0, 5.0], [5.0, 10]]
    fonction retourne la liste des intervals de pLstI pour lesquels il y a une racine
    ou une liste vide sinon
    """
    res = []
    for interval in pLstI:
        if fRacine(pF, interval) == 1:
            res.append(interval)
    return res

print(fRacines(fExo1(6), [[-10, -5.0], [-5.0, 0.0], [0.0, 5.0], [5.0, 10]]))

def fChercherRacines(pF,pL):
    """
    fonction qui va découper les intervalles jusqu'à trouver une racine.
    elle s'arrête également une fois que l'interval a été décomposé en 10 sous intervals
    elle retourne le nombre correspondant au nombre de sous interval qu'il a fallu créer pour trouver une racine
    ou -1 si aucune racine n'a pu être trouvée'
    """
    return False
    
"""
A faire 

Tester chaque fonction et faire apparaitre tous les cas possibles
"""
