class DoublyLinkedList:
    class Cell:
        def __init__(self, value, prev=None, next=None):
            self.value = value
            self.next = next
            self.prev = prev

        def __repr__(self):
            return repr(self.value)

        def __eq__(self, other):
            return self.value == other

        def __lt__(self, other):
            return self.value < other

        def __le__(self, other):
            return self.value <= other

        def __gt__(self, other):
            return self.value > other

        def __ge__(self, other):
            return self.value >= other

    def __init__(self):
        self.head = None
        self.tail = None
        self.len = 0

    def find(self, value, cell = None):
        """ Return the first cell containing the value value from the cell cell
        Return None if value not found
        cell must be a valid cell or None to search from the beginning
        Complexity : up to linear in container size, constant in size"""

        cell = cell if cell else self.head
        while cell and cell.value != value:
            cell = cell.next
        return cell

    def rfind(self, value, cell = None):
        """ Return the last cell containing the value value from the cell cell in reverse order
        Return None if value not found
        cell must be a valid cell or None to search from the end
        Complexity : up to linear in container size, constant in size"""

        cell = cell if cell else self.tail
        while cell and cell.value != value:
            cell = cell.prev
        return cell

    def cell(self, idx):
        """ Return the cell at index idx, idx might be negative causing reverse search
        Raise an IndexError if idx is out of range
        Complexity : up to linear in container size, constant in size"""

        if idx < -self.len or idx >= self.len:
            raise IndexError("list index out of range")
        cell = self.head if idx >= 0 else self.tail
        for _ in range(abs(idx+1)):
            cell = cell.next if idx > 0 else cell.prev
        return cell

    def insert(self, value, cell=None):
        """ Add and return a new cell after the cell cell with value value
        cell must be a valid cell or None
        If cell is None add as first element
        Complexity : constant in time, constant in size"""

        current = DoublyLinkedList.Cell(value, cell)
        if cell:
            current.next = cell.next
            cell.next = current
        else:
            current.next = self.head
            self.head = current
        
        if current.next:
            current.next.prev = current
        else:
            self.tail = current
        self.len += 1
        return current

    def append(self, value):
        """ Add and return a new cell at the end with value value
        Complexity : constant in time, constant in size"""

        return self.insert(value, self.tail)

    def erase(self, cell):
        """ Remove and return the cell cell
        cell must be a valid cell
        Complexity : constant in time, constant in size"""

        if cell.next:
            cell.next.prev = cell.prev
        else:
            self.tail=cell.prev

        if cell.prev:
            cell.prev.next = cell.next
        else:
            self.head = cell.next
        self.len -= 1
        return cell

    def pop(self):
        """ Remove and return the last cell
        List must be none empty
        Raise an IndexError if list empty
        Complexity : constant in time, constant in size"""

        if self.len == 0:
            raise IndexError('erase from empty list')

        return self.erase(self.tail)
    

    def remove(self, value):
        """ Remove and return the first cell containing the value value
        Raise a ValueError if value not in list
        Complexity : linear in container size, constant in size"""

        cell = self.find(value)
        if cell is None:
            raise ValueError('list.remove(value): value not in list')

        return self.erase(cell)


    def extend(self, iterable):
        """ Add a list of new cells from iterable
        Return the last cell inserted
        Complexity : linear in the sum of both container sizes, linear in iterable"""

        for i in iterable:
                cell = self.insert(i, self.tail)
        return cell

    def clear(self):
        """ Clear the list, remove all elements
        Complexity : /!\ linear in container size (garbage collector), constant in size"""

        self.head = None
        self.tail = None
        self.len = 0

    def reverse(self):
        """ Reverse the order of the elements in the list container
        Complexity : linear in container size, constant in size"""

        cell = self.head 
        self.head, self.tail = self.tail, self.head
    
        while cell :
            cell.prev, cell.next = cell.next, cell.prev
            cell = cell.prev

    def __repr__(self):
        """ Magic method to print the list : print(list)
        Complexity : linear in container size (both space and time)"""

        cells = []
        cells.extend(self) # possible because __iter__ is defined : allow to iterate the list automatically 
        cells = [repr(x) for x in cells]
        return ' <-> '.join(cells)

    def __iter__(self):
        """ Magic method to iterate in the list : ex. for i in list
        Complexity : linear in container size (both space and time)"""

        node = self.head
        while node:
            yield node
            node = node.next

    def __reversed__(self):
        """ Magic method to iterate in the list in reverse order
        Complexity : linear in container size (both space and time)"""

        node = self.tail
        while node:
            yield node
            node = node.prev

    def __len__(self):
        """ Magic method to get the length of the list : len(list)
        Complexity : constant (both space and time)"""
        return self.len

if __name__ == '__main__':
    lc = DoublyLinkedList()
    lc.append(1)
    lc.pop()
    lc.append(2)
    lc.append(3)
    lc.append(4)
    lc.append(5)
    current = lc.append(6)
    lc.append(5)
    lc.insert(8, current)
    lc.remove(5)
    lc.pop()
    lc.append(2.5)
    print(lc)

    print(min(lc))
    print(max(lc))

    print(lc)
    lc.reverse()
    print(lc)

    print(len(lc))
    print('found' if lc.find(3) else 'not found')
    for i in reversed(lc):
        print(i)