class SentinelLinkedList:
    class Cell:
        def __init__(self, value, cell=None):
            self.value = value
            self.next = cell

        def __repr__(self):
            return repr(self.value)

        def __eq__(self, other):
            return self.value == other

        def __lt__(self, other):
            return self.value < other

        def __le__(self, other):
            return self.value <= other

        def __gt__(self, other):
            return self.value > other

        def __ge__(self, other):
            return self.value >= other

    def __init__(self):
        self.head = SentinelLinkedList.Cell(None)
        self.len = 0

    def find(self, value, cell = None):
        """ Return the first cell containing the value value from the cell cell
        Return None if value not found
        cell must be a valid cell or None to search from the beginning
        Complexity : up to linear in container size, constant in size"""

        cell = cell if cell else self.head.next
        while cell and cell.value != value:
            cell = cell.next
        return cell

    def cell(self, idx):
        """ Return the cell at index idx or the sentinel if idx = -1
        Raise an IndexError if idx is out of range
        Complexity : up to linear in container size, constant in size"""

        if idx < -1 or idx >= self.len:
            raise IndexError("list index out of range")
        cell = self.head
        for _ in range(idx+1):
            cell = cell.next
        return cell

    def insert(self, value, cell=None):
        """ Add and return a new cell after the cell cell with value value
        cell must be a valid cell or None or the sentinel
        If cell is None add as first element

        Complexity : constant in time, constant in size"""
        cell = cell if cell else self.head
        cell.next = SentinelLinkedList.Cell(value, cell.next)
        self.len += 1
        return cell.next

    def append(self, value):
        """ Add and return a new cell at the end with value value
        Complexity : linear in container size, constant in size"""

        return self.insert(value, self.cell(self.len - 1))

    def erase(self, cell=None):
        """ Remove and return the cell after the cell cell
        List must be none empty and cell must be a valid, not the last, cell or None or the sentinel
        If cell is None remove the first element
        Raise an IndexError if list empty or cell is the last cell
        Complexity : constant in time, constant in size"""

        if self.len == 0:
            raise IndexError('erase from empty list')

        cell = cell if cell else self.head
        
        if cell.next is None:
            raise IndexError('erase from last cell')
       
        deleted = cell.next
        cell.next = deleted.next 
        self.len -= 1
        return deleted

    def pop(self):
        """ Remove and return the last cell
        List must be none empty
        Raise an IndexError if list empty
        Complexity : linear in container size, constant in size"""

        return self.erase(self.cell(self.len-2) if self.len > 1 else None)

    def remove(self, value):
        """ Remove and return the first cell containing the value value
        Raise a ValueError if value not in list
        Complexity : linear in container size, constant in size"""

        # cell = self.find(value) -> not possible because we forgot the previous
        cell = self.head.next
        previous = self.head
        while cell and cell.value != value:
            previous = cell
            cell = cell.next
        if cell is None:
            raise ValueError('list.remove(value): value not in list')
        self.erase(previous)
        return cell

 

    def extend(self, iterable):
        """ Add a list of new cells from iterable
        Return the last cell inserted
        Complexity : linear in the sum of both container sizes, linear in iterable"""

        cell = self.cell(self.len-1)
        for i in iterable:
                cell = self.insert(i, cell)
        return cell

    def clear(self):
        """ Clear the list, remove all elements
        Complexity : /!\ linear in container size (garbage collector), constant in size"""

        self.head.next = None
        self.len = 0

    def reverse(self):
        """ Reverse the order of the elements in the list container
        Complexity : linear in container size, constant in size"""

        if self.len > 1:
            last = self.head.next
            cell = last.next
            while cell :
                last.next = cell.next
                cell.next = self.head.next
                self.head.next = cell
                cell = last.next

    def __repr__(self):
        """ Magic method to print the list : print(list)
        Complexity : linear in container size (both space and time)"""

        cells = []
        cells.extend(self) # possible because __iter__ is defined : allow to iterate the list automatically 
        cells = [repr(x) for x in cells]
        return ' -> '.join(cells)

    def __iter__(self):
        """ Magic method to iterate in the list : ex. for i in list
        Complexity : linear in container size (both space and time)"""

        node = self.head.next
        while node:
            yield node
            node = node.next

    def __len__(self):
        """ Magic method to get the length of the list : len(list)
        Complexity : constant (both space and time)"""
        return self.len

if __name__ == '__main__':
    lc = SentinelLinkedList()
    lc.append(1)
    lc.pop()
    lc.append(2)
    lc.append(3)
    lc.append(4)
    lc.append(5)
    current = lc.append(6)
    lc.append(5)
    lc.insert(8, current)
    lc.remove(5)
    lc.pop()
    lc.append(2.5)
    print(lc)

    print(min(lc))
    print(max(lc))

    print(lc)
    lc.reverse()
    print(lc)

    print(len(lc))
    print('found' if lc.find(3) else 'not found')