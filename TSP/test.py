# from csv import reader
import os
import random
# from GeneticTSPGui import PVC_Genetique_GUI
# from Population import Population
# from Trajet import Trajet
# from Ville import Ville
import copy
import numpy as np

import os
from math import sqrt
from pickletools import read_string1 
import random
import csv
import copy
from this import d

from tkinter import *
# from GeneticTSPGui import PVC_Genetique_GUI 

# Common functions
def generer_villes(nb_villes = 20):
    villes = []
    for _ in range(nb_villes):
        villes.append(Ville(random.randint(0,100000), random.randint(0,300), random.randint(0,300)))
    return villes

def lire_csv(file_name):
    with open(file_name, 'r') as file:
        reader = csv.reader(file)
        villes = []
        for row in reader:
            villes.append(Ville(int(row[0]), int(row[1]), int(row[2])))
        return villes

class Trajet:

    def __init__(self, villes = []):
        if villes != []:
            copy_list = villes.copy()
            self.villes = random.sample(copy_list, len(copy_list))
        else:
            self.villes = []
        self.longueur = 0

    def __lt__(self, other):
        return self.longueur < other.longueur
    def __gt__(self, other):
        return self.longueur > other.longueur
    def __eq__(self, other):
        return self.longueur == other.longueur

    def calc_longueur(self):
        for i in range(len(self.villes)-1):
            if len(self.villes) != i:
                self.longueur += self.villes[i].distance_vers(self.villes[i+1])

    def est_valide(self):
        return len(set([ville.nom for ville in self.villes])) == len(self.villes)

    # def est_valide(self):
    #     for i in self.villes:
    #         if self.villes.count(i.nom) > 0:
    #             return False
    #     return True

    def __str__(self):
        return str([str(v) for v in self.villes])

class Ville:

    def __init__(self, nom, x, y,):
        self.x = x
        self.y = y
        self.nom = nom
    
    def distance_vers(self, autre_ville):
        result = sqrt((self.x - autre_ville.x)**2 + (self.y - autre_ville.y)**2)
        return result

    def __str__(self):
        return str(self.nom)

class Population:

    def __init__(self):
        self.trajets = []

    def initialiser(self, taille, liste_villes):
        for _ in range(taille):
            t = Trajet(liste_villes)
            t.calc_longueur()
            self.trajets.append(t)

    def ajouter(self, trajet):
        trajet.calc_longueur()
        self.trajets.append(trajet)

    def meilleur(self):
        trajet = self.trajets[0]
        for i in range(len(self.trajets)):
            if trajet.longueur > self.trajets[i].longueur:
                trajet.longueur = self.trajets[i].longueur
        return trajet

    def __str__(self):
        return str([str(v) for v in self.trajets])


class PVC_Genetique:
  
    def __init__(self, villes, population = 40, nbr_generation = 100, elitisme = True, mut_proba = 0.3):
        self.villes = villes
        self.population = population
        self.nbr_generation = nbr_generation
        self.elitisme = elitisme
        self.mut_proba = mut_proba
        self.gui = PVC_Genetique_GUI(self.villes)

    def clear_term(self):
        os.system('cls' if os.name=='nt' else 'clear')

    def croiser(self,parent1, parent2):
        villes1 = parent1.villes
        villes2 = parent2.villes
        half = len(villes1)//2
        enfant = Trajet(villes1[:half] + villes2[half:])
        if not enfant.est_valide():
            enfant.villes = set(enfant.villes)
            villes_manquantes = []
            for ville in self.villes:
                if ville.nom not in [loop_city.nom for loop_city in enfant.villes]:
                    villes_manquantes.append(ville)
            enfant = Trajet(list(enfant.villes) + villes_manquantes)
        enfant.calc_longueur()
        return enfant
            
    def muter(self, trajet):
        for i in range(1):
            i1 = random.randint(0,len(trajet.villes)-1)
            i2 = random.randint(0,len(trajet.villes)-1)
            trajet.villes[i1], trajet.villes[i2] = trajet.villes[i2], trajet.villes[i1]
        trajet.calc_longueur()
        return trajet
        
    # On ne garde que la moitié des trajets pas ordre croissant de distance    
    def selectionner(self, population):
        return sorted(population.trajets[::])[::5]
        
    def evoluer(self,population):
        initial_length = len(population.trajets)
        trajets = self.selectionner(population)
        new_pop = Population()
        start = len(trajets)-1
        for i in range(start, initial_length - 1):
            needMutate = random.randint(0, 100) < (self.mut_proba * 100)
            if(needMutate):
                trajets.append(self.muter(copy.deepcopy(trajets[i-start])))
            else:
                trajets.append(self.croiser(population.meilleur(), trajets[i-start-1]))
        [new_pop.ajouter(trajet) for trajet in trajets]
        return new_pop

    def executer(self, afficher = True):
        population = Population()
        population.initialiser(self.population, self.villes)
        best = population.meilleur()
        for i in range(self.nbr_generation):
            population = self.evoluer(population)
            # print(population.meilleur().villes)
            if best > population.meilleur():
                best = population.meilleur()
            if(afficher):
                # for trajet in population.trajets:
                self.gui.afficher(best,population.meilleur())
        #os.system('clear')
        self.gui.window.mainloop()

def main():
    # A compléter
    villes = lire_csv('30.csv')
    program = PVC_Genetique(villes,population=10, nbr_generation=15000, elitisme=True, mut_proba=0.3)
    program.executer()