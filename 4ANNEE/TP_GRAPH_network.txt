import networkx as nx
import numpy as np

G = nx.gnm_random_graph(10, 15, seed=2)
while not nx.is_connected(G):
    G = nx.gnm_random_graph(10, 15, seed=2)


def display_G():
    nx.draw(G, with_labels=True, font_weight='bold')  

display_G()

def BFSTraversal(G, s):
    visited = [False] * G.number_of_nodes()
    queue = []
    queue.append(s)
    visited[s] = True
    res = []
    while queue:
        s = queue.pop(0)
        res.append(s)
        print(s, res, G[s])
        for i in G[s]:
            if visited[i] == False:
                queue.append(i)
                visited[i] = True
    return res

BFSTraversal(G, 3)


def DFSTraversal(G, s):
    visited = [False] * G.number_of_nodes()
    queue = []
    queue.append(s)
    visited[s] = True
    res = []
    while queue:
        s = queue.pop()
        res.append(s)
        for i in G[s]:
            if visited[i] == False:
                queue.append(i)
                visited[i] = True
    return res



# On doit ajouter visited pour savoir quel node nous avons déjà visiter et le résultat final

def DFSTraversalRec(G, s, visited):
    if s not in visited:
        visited.append(s)
        for n in list(G.neighbors(s)):
            DFSTraversalRec(G, n, visited)
        return visited



H = nx.gnm_random_graph(10, 15)
while nx.is_connected(H) == True:
    H = nx.gnm_random_graph(10, 15)
nx.draw(H, with_labels=True, font_weight='bold')  




def isConnected(G):
    for n in G.nodes():
        size = DFSTraversal(G, n)
        if len(size) != G.number_of_nodes(): return False
    return True

# Pour le graph G return True
print(isConnected(G))

# Pour le graph H return False
print(isConnected(H))



def connectedComponents(G):
    res = []
    min = [False] * G.number_of_nodes()
    for n in G.nodes():
        size = DFSTraversal(G, n)
        if min[len(size)] == False: 
            min[len(size)] = True
            res.append(size)
    return res

connectedComponents(H)


def Kruskal(G):
    edges_sorted = sorted(G.edges(data=True), key=lambda node: node[2].get('weight', 1))
    final = []
    graph = nx.Graph()
    graph.add_edge(edges_sorted[0][0], edges_sorted[0][1])
    for node in edges_sorted:
        graph.add_edge(node[0], node[1])
        if not nx.is_forest(graph):
            graph.remove_edge(node[0], node[1])
        else:
            final.append(node)
    return final    
Kruskal(G)



def covering_graph(G):
    nodes = Kruskal(G)
    graph = nx.Graph()
    for node in nodes:
        graph.add_edge(node[0], node[1], weight=node[2]['weight'])
    return graph

A = covering_graph(G)
pos = nx.spring_layout(A)
nx.draw_networkx(A,pos)
labels = nx.get_edge_attributes(A,'weight')
nx.draw_networkx_edge_labels(A,pos,edge_labels=labels)


def Prim(G, s):
    MAX = 9999999
    visiter = [False] * G.number_of_nodes()
    parents = [None] * G.number_of_nodes()
    weights = [MAX] * G.number_of_nodes()
    weights[s] = 0
    sommet = s
    visiter[s] = True
    
    while False in visiter :
        neighbors = G.neighbors(sommet)

        for n in neighbors:
            weight_edge = G[sommet][n]["weight"]
            if weight_edge < weights[n] and not visiter[n]:
                weights[n] = weight_edge
                parents[n] = sommet
        
        w_min = MAX
        for i in list(G.nodes):
            if not visiter[i]:            
                if weights[i] <= w_min:
                    sommet = i
                    w_min = weights[i]
        visiter[sommet] = True

    F = nx.Graph();
    for n in list(G.nodes):
      if parents[n] != None:
        F.add_edge(n,parents[n])
        F[n][parents[n]]['weight'] = weights[n]
    return F

A = Prim(G, 6)

pos = nx.spring_layout(A)
nx.draw_networkx(A,pos)
labels = nx.get_edge_attributes(A,'weight')
nx.draw_networkx_edge_labels(A,pos,edge_labels=labels)