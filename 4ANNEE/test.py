"""Class ARB contains Node"""
class ARB:

    class Node():
        def __init__(self, value):
            """Init Node with left, right and value for attributes"""
            self.left = None
            self.right = None
            self.value = value

    def __init__(self, data):
        """Init our ARB with a first Node"""
        self.__root =  ARB.Node(data)
    
    def _getRoot(self):
        """Getter root"""
        return self.__root

    def _isEmpty(self):
        """Check if root have no children in left and right with simple condtion"""
        return True if self.__root.left == None and self.__root.right == None else False

    def _exist(self, value):
        """Check if value exit in the tree with loop over it
            return True if exit and False if not
        """
        file = []
        file.append(self.__root)
        while len(file) > 0:
            n = file.pop(0) 
            if n.left != None:
                file.append(n.left)                
            if n.right != None:
                file.append(n.right)
            if n.value == value:
                return True
        return False

    def _insert(self, value):
        """
        Insert new value in the tree 
        Verif if the tree was initialised
        And put the value on the correct branch by checking for right or left
        """
        if self.__root is None:
            self.__root = ARB.Node(value)
            return 
        n = self.__root
        prev = n
        """Go to sub tree right or left depend of the value"""
        while n is not None:
            prev = n
            if n.value < value:
                n = n.right
            elif n.value > value:
                n  = n.left
            else:
                return
        if prev.value < value:
            prev.right = ARB.Node(value)
        else:
            prev.left = ARB.Node(value)

    def _displayPrefix(self):
        """Display with prefix method non recursive"""
        file = []
        file.append(self.__root)
        while len(file) > 0:
            n = file.pop(0) 
            if n.left != None:
                file.append(n.left)
            if n.right != None:
                file.append(n.right)
            if n.value != None:
                print(n.value)

    def __min_left(self, node):
        """Go deep in node on left side"""
        n = node
        while n.left:
            n = n.left
        return n

    def __max_right(selfn, node):
        """Go deep in node on right side"""
        n = node
        while n.right:
            n = n.right
        return n

    def _clear(self):
        """Reset the tree"""
        self.__root = ARB.Node(None)

    def _displayPostfix(self):
        """Display with postfix metthod recursive"""
        self.__parcoursPostFix(self.__root)
    def __parcoursPostFix(self, node):
        if node is not None:
            self.__parcoursPostFix(node.left)
            self.__parcoursPostFix(node.right)
            print(node.value)

    def _displayInfix(self):
        """Display with infix metthod recursive"""   
        self.__parcoursInfix(self.__root)
    def __parcoursInfix(self, node):
        if node is not None:
            self.__parcoursInfix(node.left)
            print(node.value)
            self.__parcoursInfix(node.right)

    def _displayPrefixRecu(self):
        """Display with prefix metthod recursive"""
        self.__parcoursPrefixRecu(self.__root)
    def __parcoursPrefixRecu(self, node):
        if node is not None:
            print(node.value)
            self.__parcoursPrefixRecu(node.left)
            self.__parcoursPrefixRecu(node.right)
    
    def _intersection(self, autre_arb):
        file = []
        nodes_arb1 = []
        file.append(self.__root)
        """Loop over tree and put all nodes in a list then compare the two values
        to re create the new tree by soustraction 
        """
        while len(file) > 0:
            n = file.pop(0) 
            if n.left != None:
                file.append(n.left)
            if n.right != None:
                file.append(n.right)
            nodes_arb1.append(n)
        file = []
        nodes_arb2 = []
        file.append(autre_arb._getRoot())
        while len(file) > 0:
            n = file.pop(0) 
            if n.left != None:
                file.append(n.left)
            if n.right != None:
                file.append(n.right)
            nodes_arb2.append(n)
        new_arb = []
        lenMax = max(len(nodes_arb2), len(nodes_arb1))
        for i in range(lenMax):
            try:
                if self._exist(nodes_arb2[i].value):
                    new_arb.append(nodes_arb2[i])
            except:
                continue

        self.__root = ARB.Node(new_arb[0].value)
        for i in range(1,len(new_arb)):
            self._insert(new_arb[i].value) 

    def _union(self, autre_arb):
        """Addition of two tree"""
        file = []
        nodes = []
        file.append(autre_arb.__root)
        while len(file) > 0:
            n = file.pop(0) 
            if n.left != None:
                file.append(n.left)
            if n.right != None:
                file.append(n.right)
            nodes.append(n)
        for node in nodes:
            self._insert(node.value) 

    def _displayBfs(self):
        """Display with Bfs method"""
        list = [self.__root]
        while len(list) > 0:
            list = [e.left for e in list if e.left] + \
                    [e.right for e in list if e.right]

    def _delete_ite(self, key):
        """Delete element in the tree with iterative method"""
        change = False
        if not self._exist(key):
            return
        file = []
        nodes = []
        file.append(self.__root)
        """O child"""
        while len(file) > 0:
            n = file.pop(0) 
            if n.left != None:
                file.append(n.left)
            if n.right != None:
                file.append(n.right)
            nodes.append(n)
        self.__root = ARB.Node(self.__root.value)  
        """Get the the few and re create the tree"""   
        for node in nodes:
            if not (((node.left is None) and (node.right is None)) and (node.value == key)):
                self._insert(node.value) 
            else:
                change = True
        if change: return 0
        """One child"""
        self.__root = ARB.Node(self.__root.value)  
        """Replace the key by the child element and re create the tree"""   
        for node in nodes:
            if not (((node.left is None) ^ (node.right is None)) and (node.value == key)):
                self._insert(node.value) 
            else:
                node = node.left if node.left is not None else node.right
                self._insert(node.value)
                change = True
        if change: return 0
        """Two childs case"""
        file = []
        values = []
        file.append(self.__root)
        while len(file) > 0:
            n = file.pop(0) 
            if n.left != None:
                file.append(n.left)
            if n.right != None:
                file.append(n.right)
            values.append(n.value)
        for node in nodes:
            if node.value == key:
                """Search the last node in right tree"""
                last_right = self.__max_right(node)
                last_val = last_right.value
                """Search the last node in right and left tree"""
                if last_right.left != None:
                    last_right_left = self.__min_left(last_right)
                    last_val = last_right_left.value
                """Re create the tree by replacing last element with the key"""
                values.remove(last_val)
                i = values.index(node.value)
                values = values[:i]+[last_val]+values[i+1:]
                self.__root = ARB.Node(self.__root.value) 
                for val in values:
                    self._insert(val)
                return

    def _delete_recu(self, value):
        """Delete element in the tree recursive method"""
        self.__root = self.__delete_recu_node(self.__root, value)

    def __delete_recu_node(self, root, value): 
        """Check if need to go left or right for the value"""
        if value < root.value:
            root.left = self.__delete_recu_node(root.left, value)
        elif(value > root.value):
            root.right = self.__delete_recu_node(root.right, value)
        else:
            """One child case or 0 child"""
            if root.left is None:
                temp = root.right
                root = None
                return temp
            elif root.right is None:
                temp = root.left
                root = None
                return temp
            """Two childs case"""
            temp = self.__max_right(root.right)
            if temp.left is None:
                root.value = temp.value
                root.right = self.__delete_recu_node(root.right, temp.value)
            else:
                temp2 = self.__min_left(temp)
                root.value = temp2.value
                root.right = self.__delete_recu_node(root.right, temp2.value)
        return root

    def __height(self, node):
        if node is None:
            return 0   
        else:
            leftD = self.__height(node.left)
            rightD = self.__height(node.right)
            return leftD + 1 if leftD > rightD else rightD + 1

    def _heightTree(self):
        """Get the height of a tree recursive"""
        return self.__height(self.__root)

    def _exist_index(self, value):
        """Check if value exit in the tree with loop over it
            return True if exit and False if not
        """
        file = []
        index = 0
        file.append(self.__root)
        while len(file) > 0:
            index = index + 1
            n = file.pop(0) 
            if n.left != None:
                file.append(n.left)                
            if n.right != None:
                file.append(n.right)
            if n.value == value:
                return index
        return index


"""Implementation of the class ARB with all this differents methods"""


"""Creation of the tree"""
tree = ARB(5)

"""Insert and delete"""
tree._insert(3)
tree._insert(6)
tree._insert(8)
tree._insert(9)
tree._insert(10)
tree._insert(12)
tree._insert(11)
tree._insert(7)
tree._insert(1)

tree2 = ARB(5)
tree2._insert(3)
tree2._insert(6)
tree2._insert(9)
tree2._insert(7)
tree2._insert(10)
tree._delete_recu(8)

"""Clear, exist and isEmpty"""
# tree._isEmpty()
# tree._exist(3)
# tree._clear()

"""Interaction & Union"""
# tree._intersection(tree2)
# tree._union(tree2)

"""Height"""
tree._displayPrefix()

"""Display"""
# tree._displayPrefixRecu()
# tree._displayPostfix()
# tree._displayInfix()
# tree._displayPrefix()